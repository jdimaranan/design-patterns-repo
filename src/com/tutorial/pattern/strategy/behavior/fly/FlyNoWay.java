package com.tutorial.pattern.strategy.behavior.fly;

public class FlyNoWay implements FlyBehavior {

	@Override
	public void fly() {
		System.out.println("I'm doing FlyBehavior : FlyNoWay");
	}

}
