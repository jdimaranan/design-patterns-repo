package com.tutorial.pattern.strategy.behavior.fly;

public class FlyWithNoWings implements FlyBehavior {

	public void fly() {
		System.out.println("I'm doing FlyBehavior : FlyWithNoWings");
	}

}
