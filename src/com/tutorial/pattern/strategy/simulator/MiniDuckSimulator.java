package com.tutorial.pattern.strategy.simulator;

import com.tutorial.pattern.strategy.behavior.fly.FlyRocketPowered;
import com.tutorial.pattern.strategy.model.Duck;
import com.tutorial.pattern.strategy.model.MallardDuck;
import com.tutorial.pattern.strategy.model.ModelDuck;

public class MiniDuckSimulator {

	public static void main(String[] args) {
		Duck mallard = new MallardDuck();
		
		mallard.performFly();
		mallard.performQuack();
		
		Duck model = new ModelDuck();
		//
		model.performFly();
		model.performQuack();
		//Change Fly behavior of model duck dynamically
		model.setFlyBehaviorDynamically(new FlyRocketPowered());
		model.performFly();
		
	}

}
