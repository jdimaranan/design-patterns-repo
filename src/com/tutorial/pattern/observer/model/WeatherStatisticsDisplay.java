package com.tutorial.pattern.observer.model;

import java.util.ArrayList;

public class WeatherStatisticsDisplay implements DisplayElement, Observer {
	private WeatherData weatherData;
	private float averageTemperature;
	private float averageHumidity;
	private float averagePressure;
	private ArrayList<Float> temperatureList = new ArrayList<Float>();
	private ArrayList<Float> humidityList = new ArrayList<Float>();
	private ArrayList<Float> pressureList = new ArrayList<Float>();
	
	public WeatherStatisticsDisplay(WeatherData weatherData) {
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}
	
	@Override
	public void update(float temperature, float humidity, float pressure) {
		updateTemperature(temperature);
		updateHumidity(humidity);
		updatePressure(pressure);
		display();

	}

	@Override
	public void display() {
		System.out.println("WeatherStatisticsDisplay. Average temperature at " + this.averageTemperature + " F degrees.  Average humidity at % " + this.averageHumidity 
				+ " . Average pressure at " + this.averagePressure);
	}
	
	public void updateTemperature(float temperature) {
		this.temperatureList.add(temperature);
		float averageTemperature = 0.0f;
		int listSize = temperatureList.size();
		
		for (float currentTemperatureAtIndex : temperatureList) {
			averageTemperature = averageTemperature + currentTemperatureAtIndex;
		}
		
		averageTemperature = averageTemperature / listSize;
		this.averageTemperature = averageTemperature;
	}
	
	public void updateHumidity(float humidity) {
		this.humidityList.add(humidity);
		
		float averageHumidity = 0.0f;
		int listSize = humidityList.size();
		
		for (float currentHumidityAtIndex : humidityList) {
			averageHumidity = averageHumidity + currentHumidityAtIndex;
		}
		averageHumidity = averageHumidity / listSize;
		this.averageHumidity = averageHumidity;
		
	}
	
	public void updatePressure(float pressure) {
		this.pressureList.add(pressure);
		
		float averagePressure = 0.0f;
		int listSize = pressureList.size();
		
		for (float currentPressureAtIndex : pressureList) {
			averagePressure = averagePressure + currentPressureAtIndex;
		}
		averagePressure = averagePressure / listSize;
		this.averagePressure = averagePressure;
	}

}
