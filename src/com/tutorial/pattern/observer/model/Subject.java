package com.tutorial.pattern.observer.model;

public interface Subject {
	public void registerObserver(Observer anyObserver);
	public void removeObserver(Observer anyObserver);
	
	/**
	 * Used to update all the current observers whenever Subject state changes
	 */
	public void notifyObservers();
}
